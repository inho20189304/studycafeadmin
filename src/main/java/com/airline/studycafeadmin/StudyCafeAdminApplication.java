package com.airline.studycafeadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StudyCafeAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(StudyCafeAdminApplication.class, args);
    }

}
