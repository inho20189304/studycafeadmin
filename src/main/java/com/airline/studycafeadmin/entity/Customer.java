package com.airline.studycafeadmin.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
@Column(nullable = false, length = 20)
    private String customerName;
@Column(nullable = false, length = 20)
    private String customerPhone;
@Column(nullable = false)
    private Integer seatNumber;
@Column(nullable = false)
    private Integer pass;
@Column(nullable = false)
    private LocalDateTime admissionTime;
@Column(nullable = false)
    private LocalDateTime checkOutTime;

}
