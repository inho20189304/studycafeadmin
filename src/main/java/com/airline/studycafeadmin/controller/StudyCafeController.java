package com.airline.studycafeadmin.controller;

import com.airline.studycafeadmin.model.CustomerAddTimeRequest;
import com.airline.studycafeadmin.model.CustomerChangeSeatRequest;
import com.airline.studycafeadmin.model.CustomerItem;
import com.airline.studycafeadmin.model.CustomerRegister;
import com.airline.studycafeadmin.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/customer")
public class StudyCafeController {

    private final CustomerService customerService;

    @PostMapping("/register")
    public String setCustomer(@RequestBody @Valid CustomerRegister register) {
        customerService.setCustomer(register);

        return "등록완료";
    }
    @GetMapping("/all")
    public List<CustomerItem> getCustomers() {
        List<CustomerItem> result = customerService.getCustomers();

        return result;
    }
    @PutMapping("/addtime/id/{id}")
    public String putAddTime(@PathVariable long id, @RequestBody @Valid CustomerAddTimeRequest request) {
        customerService.putAddTime(id, request);

        return "연장완료";
    }
    @PutMapping("/change/id/{id}")
    public String putChangeSeat(@PathVariable long id, @RequestBody @Valid CustomerChangeSeatRequest request) {
        customerService.putChangeSeat(id, request);

        return "좌석 변경 완료";
    }
}
