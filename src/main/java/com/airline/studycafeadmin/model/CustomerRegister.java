package com.airline.studycafeadmin.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRegister {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 11, max = 20)
    private String customerPhone;
    @NotNull
    private Integer seatNumber;
    @NotNull
    private Integer pass;
}
