package com.airline.studycafeadmin.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerItem {
    private Long id;
    private String customerName;
    private String customerPhone;
    private Integer seatNumber;
    private Integer pass;
}
