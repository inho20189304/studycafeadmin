package com.airline.studycafeadmin.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerChangeSeatRequest {
    private Integer seatNumber;
}
