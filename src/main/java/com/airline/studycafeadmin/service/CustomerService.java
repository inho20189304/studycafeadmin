package com.airline.studycafeadmin.service;

import com.airline.studycafeadmin.entity.Customer;
import com.airline.studycafeadmin.model.CustomerAddTimeRequest;
import com.airline.studycafeadmin.model.CustomerChangeSeatRequest;
import com.airline.studycafeadmin.model.CustomerItem;
import com.airline.studycafeadmin.model.CustomerRegister;
import com.airline.studycafeadmin.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRegister register) {
        Customer addData = new Customer();
        addData.setCustomerName(register.getCustomerName());
        addData.setCustomerPhone(register.getCustomerPhone());
        addData.setSeatNumber(register.getSeatNumber());
        addData.setPass(register.getPass());
        addData.setAdmissionTime(LocalDateTime.now());
        addData.setCheckOutTime(LocalDateTime.now().plusHours(register.getPass()));

        customerRepository.save(addData);
    }
    public List<CustomerItem> getCustomers() {
        List<Customer> originData = customerRepository.findAll();

        List<CustomerItem> result = new LinkedList<>();

        for (Customer item : originData) {
            CustomerItem addItem = new CustomerItem();
            addItem.setId(item.getId());
            addItem.setCustomerName(item.getCustomerName());
            addItem.setCustomerPhone(item.getCustomerPhone());
            addItem.setSeatNumber(item.getSeatNumber());
            addItem.setPass(item.getPass());

            result.add(addItem);
        }
        return result;
    }
    public void putAddTime(long id, CustomerAddTimeRequest request) {
        Customer originData = customerRepository.findById(id).orElseThrow();
        originData.setPass(request.getPass());
        originData.setCheckOutTime(originData.getAdmissionTime().plusHours(request.getPass()));

        customerRepository.save(originData);
    }
    public void putChangeSeat(long id, CustomerChangeSeatRequest request) {
        Customer originData = customerRepository.findById(id).orElseThrow();
        originData.setSeatNumber(request.getSeatNumber());

        customerRepository.save(originData);
    }

}
