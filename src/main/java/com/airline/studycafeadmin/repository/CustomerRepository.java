package com.airline.studycafeadmin.repository;

import com.airline.studycafeadmin.entity.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
